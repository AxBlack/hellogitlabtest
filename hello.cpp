/*
 * hello.cpp
 * Simple program for test git and gitlab.com
 * Простая программа для тестирования git и gitlab.com
 * 
 * Copyright 2020 Черноус Алексей <alexch82@ya.ru>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <math.h>

using namespace std;

int main(int argc, char **argv)
{
  cout << "Hello, GitLab!" << endl;
  int x = 0;
  cout << "Введите целое число: x = ";
  cin >> x;
  cout << "x = " << x << endl;
  cout << "x^5 = " << pow(x, 5) << endl;
  cout << "Обработка параметров командной строки (всего " << argc << "):" << endl;
  for (int i = 0; i <  argc; i++) {
    cout << i << ": " << argv[i] << endl;
  }
  cout << "Добавление строк для тестирования слияний GitLab" << endl;
  int a, b;
  cout << "Введите два целых числа: ";
  cin >> a >> b;
  cout << "Сумма этих чисел: " << a + b;
  return 0;
}

